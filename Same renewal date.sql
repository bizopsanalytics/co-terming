with product_own as
 (       
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain,
                c.period_end_date        
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161231
        and     c.level in ('Full', 'Starter')  
        and     b.platform in ('Server', 'Data Center')
        --and     c.billing_period = 'Annual'
),
aggie as 
(
select  customer_id, 
        count(distinct sen) as counts
from product_own
group by 1
),
end_dates as
(
select a.customer_id, b.period_end_date, b.sen
from aggie as a
left join product_own as b on a.customer_id = b.customer_id
where counts > 1
),
uniques as 
(
select customer_id, count(distinct period_end_date) as unique_dates
from end_dates
group by 1
)
select unique_dates, count(distinct customer_id)
from uniques
group by 1
order by 1 asc

;

